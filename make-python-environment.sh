#!/bin/sh

cfg="${HOME}/.gdb"
env="${cfg}/p/env"
activate="${env}/bin/activate"
pkgs='arm-gdb cmsis-svd'

if [ -d "$env" ]; then
    printf 'Environment exists: %s\n' "$env"
    printf 'Leaving it alone!\n'
    exit 0
fi

# post-deploy, this directory should really exist.
if [ -d "$cfg" ]; then
    cd "$cfg" || {
        printf 'Giving up!\n'
        exit 1
    }
    python3 -m venv "$env" || exit 1
    . "$activate" || exit 1
    for pkg in $pkgs; do
        pip install "$pkg" || exit 1
    done
    git clone https://github.com/ft/arm-gdb.git p/arm-gdb || exit 1
else
    printf 'make-python-environment.sh: %s does not exist!?\n' "$cfg"
    exit 1
fi

exit 0
